@extends('admin')

@section('content')

    <table style="width:100%">
        <tr>
            <th>Student Name</th>
            <th>Year Level</th>
            <th>Average</th>
        </tr>

    @foreach($grades as $grade)
        <tr>
            <th>{{ $grade->student->formal_name }}</th>
            <th>{{ $grade->student->yearlevel }}</th>
            <th>{{ $grade->average }}</th>
        </tr>
    @endforeach
@stop