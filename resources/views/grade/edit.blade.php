@extends('admin')

@section('content')
<form action="{{ route('student.grade.update', $student) }}" method="POST" class="form-horizontal">
    <input type="hidden" name="_method" value="PUT">

    {{--
    Laravel 5.4 Token Mismatch problem
    https://stackoverflow.com/questions/42687461/laravel-5-4-tokenmismatchexception-in-verifycsrftoken-php-line-68
    --}}
    {{ csrf_field() }}
    <div class="form-group">

        <h1>Updating Grades for {{ $student->lname }}, {{ $student->fname }}</h1>

        </br>
        </br>
        <label class="form-label" for="prelim">Prelim:</label>
        <input type="text" id="prelim" class="form-input" name="prelim" value="{{ $student->grade->prelim }}"
               placeholder="Prelim Grade"/>
        <p class="form-input-hint">{{ $errors->first('prelim') }}</p>

        <label class="form-label" for="midterm">Midterm:</label>
        <input type="text" id="midterm" class="form-input" name="midterm" value="{{ $student->grade->midterm }}"
               placeholder="Midterm"/>
        <p class="form-input-hint">{{ $errors->first('midterm') }} </p>

        <label class="form-label" for="finals">Finals:</label>
        <input type="text" id="finals" class="form-input" name="finals" value="{{ $student->grade->finals }}"
               placeholder="Final Grade"/>
        <p class="form-input-hint">{{ $errors->first('finals') }}</p>

        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>
@stop