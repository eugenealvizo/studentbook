@extends('admin')

@section('content')

    <table style="width:100%">
        <tr>
            <th>Firstname</th>
            <th></th>Lastname</th>
            <th>Age</th>
            <th>Year Level</th>
            <th>Action</th>
        </tr>
        @foreach($students as $student)
        <tr>
            <td>  {{ $student->fname }} </td>
            <td>  {{ $student->lname }} </td>
            <td>  {{ $student->age }} </td>
            <td>  {{ $student->yearlevel }}</td>
            <td>
                <a href="{{ route('student.grade.edit', $student) }}">Update Grades</a>
                <form method="POST" action="{{ route('student.destroy', $student) }}" >
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">

                    <button>Delete</button>
                </form>

                <a href="{{route('student.edit', $student)}}")> Edit Student </a>
            </td>
        </tr>
        @endforeach

    </table>

@stop

