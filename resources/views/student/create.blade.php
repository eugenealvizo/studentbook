
@extends('admin')

@section('content')

<form action="{{ route('student.store') }}" method="POST" class="form-horizontal">

    {{--
    Laravel 5.4 Token Mismatch problem
    https://stackoverflow.com/questions/42687461/laravel-5-4-tokenmismatchexception-in-verifycsrftoken-php-line-68
    --}}
    {{ csrf_field() }}
    <div class="form-group @haserror('first_name')">
        <label class="form-label" for="firstname">First Name:</label>
        <input type="text" id="firstname" class="form-input" name="fname" value="{{ old('fname') }}"
               placeholder="First Name"/>
        <p class="form-input-hint">{{ $errors->first('fname') }}</p>

        <label class="form-label" for="lastname">Last Name:</label>
        <input type="text" id="lastname" class="form-input" name="lname" value="{{ old('lname') }}"
               placeholder="Last Name"/>
        <p class="form-input-hint">{{ $errors->first('lname') }}</p>

        <label class="form-label" for="age">Age:</label>
        <input type="text" id="age" class="form-input" name="age" value="{{old('age')}}" placeholder="Age"/>
        <p class="form-input-hint">{{ $errors->first('age') }} </p>

        <label class="form-label" for="yearlevel">Year Level: </label>
        <input type="text" id="yearlevel" class="form-input" name="yearlevel" value="{{old('yearlevel')}}"
               placeholder="yearlevel"/>
        <p class="form-input-hint"> {{ $errors->first('yearlevel')}} </p>

        <button type="submit" class="btn btn-primary">Save</button>

    </div>
</form>

@stop