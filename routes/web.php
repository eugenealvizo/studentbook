<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin');
});

Route::get('/', function(){
    return view('student.create');
})->name('student.dashboard');

//Route::get('/student/show_grades',"StudentController@getGradesOfStudent")
//    ->name('student.show.grades');

Route::get('top', 'TopStudentsController')->name('top');


Route::get('/student',"StudentController@index")->name('student.index');
Route::post('student', 'StudentController@store')->name('student.store');
Route::delete('student/{student}', 'StudentController@delete')->name('student.destroy');



Route::get('student/{student}/grade/edit', "StudentGradeController@edit")->name('student.grade.edit');
Route::put('student/{student}/grade', "StudentGradeController@update")->name('student.grade.update');

Route::get('student/{student}/edit', "StudentController@edit")->name('student.edit');
Route::put('student/{student}', "StudentController@update")->name('student.update');
