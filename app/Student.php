<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $guarded = [];

    public static function boot()
    {
        static::created(function ($student) {
            $student->grade()->save(new Grades);
        });
    }

    public function getFullNameAttribute()
    {
        return $this->fname . ' ' . $this->lname;
    }

    public function getFormalNameAttribute()
    {
        return $this->lname . ', ' . $this->fname;
    }


    public function grade()
    {
        return $this->hasOne(Grades::class);
    }
}
