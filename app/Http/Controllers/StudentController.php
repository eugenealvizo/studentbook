<?php

namespace App\Http\Controllers;

use App\Grades;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function redirect;

class StudentController extends Controller
{

    public function index()
    {
        return view('student.index', [
            'students' => Student::all()
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'age' => 'required|integer',
            'yearlevel' => 'required|integer',
        ]);

        $student = Student::create($request->all(
            'fname', 'lname', 'age', 'yearlevel'
        ));

        return redirect()->route('student.index', $student);
    }


    public function edit(Student $student)
    {
        return view('student.edit',
            ["student" => $student]);
    }

    public function update(Request $request, Student $student)
    {
        static::update($request->only('prelim', 'midterm', 'finals'));
        return redirect()->route('student.index');

    }

    public function delete(Student $student)
    {

        $student->delete();

        return redirect()->back();
    }
}
