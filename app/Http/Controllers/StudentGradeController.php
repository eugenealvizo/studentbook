<?php

namespace App\Http\Controllers;

use App\Student;
use App\Grades;
use Illuminate\Http\Request;

class StudentGradeController extends Controller
{

    public function edit(Student $student)
    {
        return view('grade.edit', [
            'student' => $student
        ]);
    }

    public function update(Request $request, Student $student)
    {
        $student->grade->update($request->only('prelim', 'midterm', 'finals'));

        return redirect()->route('top');

    }
}
