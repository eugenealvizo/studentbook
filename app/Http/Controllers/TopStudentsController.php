<?php
/**
 * Created by PhpStorm.
 * User: eugenealvizo
 * Date: 13/07/2017
 * Time: 4:31 PM
 */

namespace App\Http\Controllers;

use App\Grades;

class TopStudentsController extends Controller
{
    function __invoke()
    {
        $grades = Grades::with('student')
            ->get()
            ->sortByDesc('average');

        return view('top ', [
            'grades'=> $grades
        ]);
    }

}