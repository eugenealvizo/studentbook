<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grades extends Model
{
    protected $guarded = [];

    protected $appends = ['average'];

    function getAverageAttribute()
    {
        return (($this->prelim + $this->midterm + $this->finals) / 3);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
